package Connection;

import Connection.messages.Message;
import Connection.messages.messageboard.*;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.function.Consumer;

/**
 * Így fog majd indulni, még bejelentkezés előtt.
 * Connection connection = Connection.getInstance();
 *         connection.setPORT(3333);
 *         connection.setSERVER("localhost");
 *         connection.run();
 */

/**
 * Egy client socket class, ami képes objectumok átvitelére a server sockethez
 */
public class Connection{
    public final MessageHandler messageHandler = new MessageHandler();
    /**
     * A Consumer<T> interface, egy FunctionalInterface, ami azt jelenti,
     * csak egy függvénye van. Mivel az interface-t adtuk csak meg, később lehetőség van,
     * az interface-ben található függvény megadására egy lambda függvényként.
     */
    private Consumer<Serializable> onReceiveCallback;

    private String server;
    private int port;

    private static Connection connection = new Connection();

    private Connection() {
        messageHandler.setDaemon(true);
    }

    public static Connection getInstance()
    {
        return connection;
    }

    public void startConnection(){
        messageHandler.start();
    }
    /**
     * Elküldi a server-nek az adatot.
     * @param data Egy Message típus, aminek van egy content tagja, ami Serializable
     * @throws IOException Ezt le kell kezelni amikor ezt hívjuk
     */
    public void send(Serializable data) throws IOException {
        messageHandler.out.writeObject(data);
        messageHandler.out.flush();
        //ha ez nincs, akkor a végtelenségig dobálja az EOF exception-t
        //messageHandler.out.reset();
    }
    public void stop() throws IOException {
        messageHandler.client.close();
    }

    /**
     * A "MessageHandler" class figyeli egy másik szálon, hogy a kliensnek van-e valami, a bemeneti stream-jében,
     * ha van, akkor az a server-től kapott válasz message.
     */
    private class MessageHandler extends Thread {
        private Socket client;
        private ObjectOutputStream out;

        @Override
        public void run(){
            try (Socket client = new Socket(getServer(), getPort());
            ObjectOutputStream output = new ObjectOutputStream(client.getOutputStream());
            ObjectInputStream input = new ObjectInputStream(client.getInputStream())) {
                this.client = client;
                this.out = output;

                while(true){
                    try {
                        Message msg = (Message)input.readObject();
                        //a később megadott függvény szerint jár el
                        onReceiveCallback.accept(msg);
                        //ehelyett kell az onReceive
                        //processMessage(msg);
                    } catch (IOException e) {
                        e.printStackTrace();
                        break;
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
            catch (Exception e)
            {
                onReceiveCallback.accept("Sorry!");
            }
        }
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setOnReceiveCallback(Consumer<Serializable> onReceiveCallback) {
        this.onReceiveCallback = onReceiveCallback;
    }
}
