package Connection.messages.messageboard;

import Connection.messages.Message;

/**
 * Ha a server nem tudja a kérésünket valamiért feldolgozni akkor egy ilyen típusú
 * message-t fog visszaküldeni.
 */
public class ErrorMessage extends Message {

    private String errorMsg;

    public ErrorMessage(String errorMsg){
        this.errorMsg = errorMsg;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
