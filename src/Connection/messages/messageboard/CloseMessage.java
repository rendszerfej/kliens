package Connection.messages.messageboard;

import Connection.messages.Message;

/**
 * Záró message típus, amikor a kliens "QUIT"-et ír be a console-jára,
 * akkor egy ilyen típusú üzenetet küld, aminek nem plusz adat,
 * mert a server a típusból tudja, hogy mit szeretnénk csinálni.
 */
public class CloseMessage extends Message {

}
