package Connection.messages.classes;

import java.io.Serializable;

public class Profile implements Serializable, IItem {
    int profile_id;
    String first_name, last_name, email, password, phone_number, city;

    public Profile() {
    }

    public Profile(int profile_id, String first_name, String last_name, String email, String password, String phone_number, String city) {
        this.profile_id = profile_id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.password = password;
        this.phone_number = phone_number;
        this.city = city;
    }

    @Override
    public String toString() {
        return "user id: " + profile_id + "\n" +
                "first name: " + first_name + "\n" +
                "last name: " + last_name + "\n" +
                "email: " + email + "\n" +
                "password: " + password + "\n" +
                "phone number: " + phone_number + "\n" +
                "city: " + city;
    }

    public int getProfile_id() {
        return profile_id;
    }

    public void setProfile_id(int profile_id) {
        this.profile_id = profile_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public int getID() {
        return profile_id;
    }
}
