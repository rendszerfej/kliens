package Connection.messages.classes;

import java.io.Serializable;

public class Review implements Serializable, IItem {
    int review_id, product_id,profile_id, review_value;

    public Review(int review_id, int product_id,int profile_id, int review_value) {
        this.review_id = review_id;
        this.product_id = product_id;
        this.profile_id=profile_id;
        this.review_value = review_value;
    }

    public Review() {
    }

    @Override
    public String toString() {
        return "review id: " + review_id + "\n" +
                "product_id: " + product_id + "\n" +
                "profile_id: " + profile_id + "\n" +
                "review value: " + review_value;
    }

    public int getReview_id() {
        return review_id;
    }

    public void setReview_id(int review_id) {
        this.review_id = review_id;
    }


    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getProfile_id() {
        return profile_id;
    }

    public void setProfile_id(int profile_id) {
        this.profile_id = profile_id;
    }

    public int getReview_value() {
        return review_value;
    }

    public void setReview_value(int review_value) {
        this.review_value = review_value;
    }

    @Override
    public int getID() {
        return review_id;
    }
}
