package Connection.messages.classes;

import java.io.Serializable;

public class Product implements Serializable, IItem {
    int product_id, profile_id, sub_category_id, price;
    String name, description, status, advertisement_date;
//    String image;
    //images would work as we store the whole image in a folder and then we refer to that file with a path

    public Product() {
    }

    public Product(int product_id, int profile_id, int sub_category_id, int price, String advertisement_date, String name, String description, String status/*, String image*/) {
        this.product_id = product_id;
        this.profile_id = profile_id;
        this.sub_category_id = sub_category_id;
        this.price = price;
        this.advertisement_date = advertisement_date;
        this.name = name;
        this.description = description;
        this.status = status;
//        this.image = image;
    }

    @Override
    public String toString() {
        return "product id: " + product_id + "\n" +
                "user id: " + profile_id + "\n" +
                "sub category id: " + sub_category_id + "\n" +
                "price: " + price + "\n" +
                "advertisement date: " + advertisement_date + "\n" +
                "name: " + name + "\n" +
                "description: " + description + "\n" +
                "status: " + status;
//                "image: " + image;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getProfile_id() {
        return profile_id;
    }

    public void setProfile_id(int profile_id) {
        this.profile_id = profile_id;
    }

    public int getSub_category_id() {
        return sub_category_id;
    }

    public void setSub_category_id(int sub_category_id) {
        this.sub_category_id = sub_category_id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getAdvertisement_date() {
        return advertisement_date;
    }

    public void setAdvertisement_date(String advertisement_date) {
        this.advertisement_date = advertisement_date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription(){return description;}

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int getID() {
        return product_id;
    }

//    public String getImage() {
//        return image;
//    }
//
//    public void setImage(String image) {
//        this.image = image;
//    }
}
