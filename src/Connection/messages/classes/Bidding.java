package Connection.messages.classes;

import java.io.Serializable;

public class Bidding implements Serializable, IItem {
    int bidding_id, product_id, final_price;
    String start_bidding_time, end_bidding_time;



    public Bidding() {
    }



    public Bidding(int bidding_id, int product_id, int final_price, String start_bidding_time, String end_bidding_time) {
        this.bidding_id = bidding_id;
        this.product_id = product_id;
        this.final_price = final_price;
        this.start_bidding_time = start_bidding_time;
        this.end_bidding_time = end_bidding_time;
    }

    @Override
    public String toString() {
        return "bidding id: " + bidding_id + "\n" +
                "product id: " + product_id + "\n" +
                "final price: " + final_price + "\n" +
                "start bidding time: " + start_bidding_time + "\n" +
                "end bidding time: " + end_bidding_time;
    }


    public int getBidding_id() {
        return bidding_id;
    }

    public void setBidding_id(int bidding_id) {
        this.bidding_id = bidding_id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getFinal_price() {
        return final_price;
    }

    public void setFinal_price(int final_price) {
        this.final_price = final_price;
    }

    public String getStart_bidding_time() {
        return start_bidding_time;
    }

    public void setStart_bidding_time(String start_bidding_time) {
        this.start_bidding_time = start_bidding_time;
    }

    public String getEnd_bidding_time() {
        return end_bidding_time;
    }

    public void setEnd_bidding_time(String end_bidding_time) {
        this.end_bidding_time = end_bidding_time;
    }

    @Override
    public int getID() {
        return bidding_id;
    }
}
