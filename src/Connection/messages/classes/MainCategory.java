package Connection.messages.classes;

import java.io.Serializable;

public class MainCategory implements Serializable, IItem {
    int main_category_id;
    String main_category_name;

    public MainCategory() {
    }

    public MainCategory(int main_category_id, String main_category_name) {
        this.main_category_id = main_category_id;
        this.main_category_name = main_category_name;
    }

    @Override
    public String toString() {
        return "main category id: " + main_category_id + "\n" +
                "main category name: " + main_category_name;
    }

    public int getMain_category_id() {
        return main_category_id;
    }

    public void setMain_category_id(int main_category_id) {
        this.main_category_id = main_category_id;
    }

    public String getMain_category_name() {
        return main_category_name;
    }

    public void setMain_category_name(String main_category_name) {
        this.main_category_name = main_category_name;
    }

    @Override
    public int getID() {
        return main_category_id;
    }
}
