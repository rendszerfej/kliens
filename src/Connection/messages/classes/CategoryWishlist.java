package Connection.messages.classes;

import java.io.Serializable;

public class CategoryWishlist implements Serializable, IItem {
    int sub_category_id, wishlist_id;

    public CategoryWishlist() {
    }

    public CategoryWishlist(int sub_category_id, int wishlist_id) {
        this.sub_category_id = sub_category_id;
        this.wishlist_id = wishlist_id;
    }

    @Override
    public String toString() {
        return "sub category id: " + sub_category_id + "\n" +
                "wishlist id: " + wishlist_id;
    }

    public int getSub_category_id() {
        return sub_category_id;
    }

    public void setSub_category_id(int sub_category_id) {
        this.sub_category_id = sub_category_id;
    }

    public int getWishlist_id() {
        return wishlist_id;
    }

    public void setWishlist_id(int wishlist_id) {
        this.wishlist_id = wishlist_id;
    }

    @Override
    public int getID() {
        return sub_category_id;
    }
}
