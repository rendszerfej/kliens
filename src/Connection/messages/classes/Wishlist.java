package Connection.messages.classes;

import java.io.Serializable;

public class Wishlist implements Serializable, IItem {
    int wishlist_id, profile_id;

    public Wishlist() {
    }

    public Wishlist(int wishlist_id, int profile_id) {
        this.wishlist_id = wishlist_id;
        this.profile_id = profile_id;
    }

    @Override
    public String toString() {
        return "wishlist id: " + wishlist_id + "\n" +
                "profile id: " + profile_id;
    }

    public int getWishlist_id() {
        return wishlist_id;
    }

    public void setWishlist_id(int wishlist_id) {
        this.wishlist_id = wishlist_id;
    }

    public int getProfile_id() {
        return profile_id;
    }

    public void setProfile_id(int profile_id) {
        this.profile_id = profile_id;
    }

    @Override
    public int getID() {
        return wishlist_id;
    }
}
