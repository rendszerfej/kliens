package repository;

import Connection.Connection;
import Connection.messages.classes.IItem;
import Connection.messages.types.DeleteMessage;
import Connection.messages.types.InsertMessage;

import java.io.IOException;
import java.util.Collection;

public abstract class Repository {
    protected Connection connection = Connection.getInstance();
    protected Collection<IItem> items;

    Collection<IItem> getAll() {
        return items;
    }

    public void addItem(IItem item) throws IOException {
        InsertMessage im = new InsertMessage();
        im.setContent(item);
        connection.send(item);
    }

    public void removeItem(IItem item) throws IOException {
        DeleteMessage dm = new DeleteMessage();
        dm.setContent(item);
        connection.send(item);
    }

    public void clearItems() {
        items.clear();
    }

    public IItem getItemByID(int id) {
        IItem item = null;
        for (IItem iItem : items) {
            if (iItem.getID() == id) {
                item = iItem;
                break;
            }
        }
        return item;
    }

    public void addAll(Collection<IItem> items) {
        this.items.addAll(items);
    }
}
