package sample;

import Connection.Connection;
import Connection.messages.messageboard.AckMessage;
import Connection.messages.messageboard.ErrorMessage;
import Connection.messages.messageboard.ListMessage;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    Controller controller;

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root;
        FXMLLoader loader= new FXMLLoader(getClass().getResource("sample.fxml"));
        controller = loader.getController();
        root = loader.load();

        //primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.setTitle("Kliens");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
        //connection.startConnection();
    }



    public static void main(String[] args) {
        launch(args);
    }

}
