package sample;

import Connection.messages.classes.Profile;

import java.util.ArrayList;

public class GetUser implements OtherUsers {


    public GetUser() {

    }
    public String getUserFromId(int user_id){

        for(Profile p :otherUsers){
            if(p.getProfile_id()==user_id){
                return p.getFirst_name()+" "+p.getLast_name();
            }
        }
        return "";
    }
    public Profile getProfileFromId(int id){
        for(Profile p :otherUsers){
            if(p.getProfile_id()==id){
                return p;
            }
        }
        return null;
    }
}
