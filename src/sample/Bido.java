package sample;

import Connection.Connection;
import Connection.messages.classes.Bid;
import Connection.messages.classes.Bidding;
import Connection.messages.classes.Product;
import Connection.messages.types.InsertMessage;
import Connection.messages.types.UpdateMessage;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import java.io.IOException;

public class Bido extends Pane {
    @FXML
    Label bidItemName;
    @FXML
    Label myBid;
    @FXML
    Label highestBid;
    @FXML
    TextField newBid;
    @FXML
    Button createNewBid;
    @FXML
    Label bidAlert;
    private int productID,biddingID;


    public Bido(){
        init();
        createNewBid.setOnAction(event -> {
            int mybid = Integer.parseInt(myBid.getText());
            int high = Integer.parseInt(highestBid.getText());
            if(Integer.parseInt(newBid.getText())>Integer.parseInt(highestBid.getText())){
                if(mybid!=high){
                    createNewBid();
                }
                else{
                    bidAlert.setText("Ne licitálj magadra!");
                }

            }

        });

    }

    public Bido(Product product,Bidding bidding){
        init();

    }
    public Bido(String itemname,String mybid,String highestbid){
        init();
        initBidData(itemname,mybid,highestbid);
    }
    private void init(){
        FXMLLoader loader = new FXMLLoader(getClass().getResource("bid.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (Exception e) {

        }

        this.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        newBid.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                newBid.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });

    }
    public void createNewBid(){
        int newbid = Integer.parseInt(newBid.getText());
        Connection conn = Connection.getInstance();
        InsertMessage im = new InsertMessage();
        UpdateMessage um = new UpdateMessage();
        Bidding bidding = new Bidding();
        bidding.setProduct_id(productID);
        bidding.setFinal_price(newbid);
        um.setContent(bidding);
        Bid b = new Bid();
        b.setBidding_id(biddingID);
        b.setBidder_id(Controller.loginUser.getProfile_id());
        b.setBid_value(newbid);
        im.setContent(b);
        try {
            conn.send(im);
            conn.send(um);

            highestBid.setText(String.valueOf(newbid));
            myBid.setText(String.valueOf(newbid));
        } catch (IOException e) {
            e.printStackTrace();
        }




    }
    public void initBidData(String itemname,String mybid,String highestbid){
        this.bidItemName.setText(itemname);
        this.myBid.setText(mybid);
        this.highestBid.setText(highestbid);

    }
    public void initBidData(String itemname,int mybid,int highestbid,int productID,int biddingID){
        this.bidItemName.setText(itemname);
        this.myBid.setText(String.valueOf(mybid));
        this.highestBid.setText(String.valueOf(highestbid));
        int high = Integer.parseInt(highestBid.getText());
        newBid.setText(String.valueOf(high+1));
        this.productID=productID;
        this.biddingID=biddingID;

    }
}
