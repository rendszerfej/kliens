package sample;

public class GetCategory {
    public GetCategory() {

    }
    public String getCategoryFromInt(int cat){
        switch (cat){
            case 1:
                return categories.Muszaki_Mob.toString();
            case 2:
                return categories.Muszaki_TV.toString();
            case 3:
                return categories.Muszaki_Ful.toString();
            case 4:
                return categories.SzT1.toString();
            case 5:
                return categories.SzT2.toString();
            case 6:
                return categories.SzT3.toString();
            case 7:
                return categories.HztGp1.toString();
            case 8:
                return categories.HztGp2.toString();
            case 9:
                return categories.HztGp3.toString();
            case 10:
                return categories.DandR1.toString();
            case 11:
                return categories.DandR2.toString();
            case 13:
                return categories.SandF1.toString();
            case 15:
                return categories.J1.toString();
            case 16:
                return categories.J2.toString();
            case 17:
                return categories.J3.toString();
            case 20:
                return categories.AM2.toString();
            case 21:
                return categories.Iroda1.toString();
            case 22:
                return categories.Iroda2.toString();
            case 23:
                return categories.Iroda3.toString();
            case 24:
                return categories.DandR3.toString();
            case 25:
                return categories.SandF2.toString();
            case 26:
                return categories.OandK.toString();
            case 27:
                return categories.AM1.toString();
                default:
                    return "No such category!";
        }
    }
}
