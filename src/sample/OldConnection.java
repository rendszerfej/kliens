package sample;

import javafx.application.Platform;
import javafx.concurrent.Task;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class OldConnection {
    protected Socket socket = null;
    protected DataInputStream input = null;
    protected DataOutputStream out = null;
    protected DataInputStream in = null;


    public OldConnection(String address, int port)
    {
        new Thread(()-> {
            try
            {
                socket = new Socket(address, port);
                System.out.println("Connected");

                // takes input from terminal
                input  = new DataInputStream(System.in);
                in = new DataInputStream(socket.getInputStream());
                //input  = new DataInputStream(socket.getInputStream());

                // sends output to the socket
                out    = new DataOutputStream(socket.getOutputStream());

            }
            catch(UnknownHostException u)
            {
                System.out.println(u);
            }
            catch(IOException i)
            {
                System.out.println(i);
            }

            // string to read message from input


            // keep reading until "Over" is input
            String line = "";
            while (!line.equals("Over"))
            {
                try
                {

                    line = in.readLine();
                    System.out.println(line);//.readUTF();
                    out.writeUTF(line);
                }
                catch(IOException i)
                {
                    System.out.println(i);
                }
            }


            // close the connection
            try
            {
                closingConn();
            }
            catch(IOException i)
            {
                System.out.println(i);
            }
        }).start();
        // establish a connection

    }



    protected void closingConn() throws IOException {
        out.writeChars("Over");
        input.close();
        out.close();
        socket.close();
    }
}
