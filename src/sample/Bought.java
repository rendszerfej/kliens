package sample;

import Connection.Connection;
import Connection.messages.classes.Review;
import Connection.messages.types.InsertMessage;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableBooleanValue;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import java.io.IOException;

public class Bought extends Pane {

    @FXML
    Label boughtName;
    @FXML
    Label boughtPrice;
    @FXML
    Label boughtOwner;
    @FXML
    Label boughtInitDate;
    @FXML
    Label boughtFinalDate;
    @FXML
    Label boughtCat;
    @FXML
    Button boughtRate;
    /*@FXML
    Button boughtItemMore;*/
    @FXML
    RadioButton rateOne;
    @FXML
    RadioButton rateTwo;
    @FXML
    RadioButton rateThree;
    @FXML
    RadioButton rateFour;
    @FXML
    RadioButton rateFive;
    @FXML
    Label noSelected;
    private BooleanProperty isRated=new SimpleBooleanProperty();
    private GetCategory gc = new GetCategory();
    private int selectedRate=0;
    GetUser gu = new GetUser();



    private final ToggleGroup rateButtons=new ToggleGroup();
    private Item boughtItem;

    public Item getBoughtItem() {
        return boughtItem;
    }

    public void setBoughtItem(Item boughtItem) {
        this.boughtItem = boughtItem;
        this.boughtName.setText(boughtItem.itemProduct.getName());
        this.boughtPrice.setText(String.valueOf(boughtItem.itemBidding.getFinal_price()));
        this.boughtOwner.setText(gu.getUserFromId(boughtItem.itemProduct.getProfile_id()));
        this.boughtInitDate.setText(boughtItem.itemBidding.getStart_bidding_time());
        this.boughtFinalDate.setText(boughtItem.itemBidding.getEnd_bidding_time());
        this.boughtCat.setText(gc.getCategoryFromInt(boughtItem.getItemProduct().getSub_category_id()));
        //this.boughtCat.setText(cat);

    }

    public Bought(){
        init();
        boughtRate.setOnAction(event -> {
                if(selectedRate!=0){
                    setRate(selectedRate);
                    Connection conn = Connection.getInstance();
                    InsertMessage im = new InsertMessage();
                    Review r = new Review();
                    r.setProduct_id(boughtItem.getItemProduct().getProduct_id());
                    r.setProfile_id(boughtItem.getItemProduct().getProfile_id());
                    r.setReview_value(selectedRate);
                    im.setContent(r);
                    try {
                        conn.send(im);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else{
                    noSelected.setText("Nem választottál ki értéket");
                }



        });
    }

    public Bought(String itemname, String price, String owner, String initDate, String finalDate, String cat, int rate){
        init();
        initBoughtData(itemname, price,  owner, initDate, finalDate, cat);
        setRate(rate);
    }
    private void init(){
        FXMLLoader loader = new FXMLLoader(getClass().getResource("bought.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (Exception e) {

        }

        this.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        rateOne.setToggleGroup(rateButtons);

        rateTwo.setToggleGroup(rateButtons);
        rateThree.setToggleGroup(rateButtons);
        rateFour.setToggleGroup(rateButtons);
        rateFive.setToggleGroup(rateButtons);
        isRated.setValue(true);
        boughtRate.visibleProperty().bind(isRated);
        //boughtRate.setDisable(true);
        rateButtons.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                RadioButton chk = (RadioButton)rateButtons.getSelectedToggle();
                selectedRate=Integer.parseInt(chk.getText());
            }
        });


    }

    public void initBoughtData(String itemname, String price, String owner, String initDate, String finalDate, String cat){
        this.boughtName.setText(itemname);
        this.boughtPrice.setText(price);
        this.boughtOwner.setText(owner);
        this.boughtInitDate.setText(initDate);
        this.boughtFinalDate.setText(finalDate);
        this.boughtCat.setText(cat);
    }
    public void setRate(int num){
        switch (num){
            case 1:
                rateOne.setSelected(true);
                isRated.setValue(false);
                break;
            case 2:
                rateTwo.setSelected(true);
                isRated.setValue(false);
                break;
            case 3:
                rateThree.setSelected(true);
                isRated.setValue(false);
                break;
            case 4:
                rateFour.setSelected(true);
                isRated.setValue(false);
                break;
            case 5:
                rateFive.setSelected(true);
                isRated.setValue(false);
                break;
            default:
                break;
        }
    }
    public void initBoughtData(String itemname,String price,String owner,String initDate,String finalDate,String cat,int rate){
        this.boughtName.setText(itemname);
        this.boughtPrice.setText(price);
        this.boughtOwner.setText(owner);
        this.boughtInitDate.setText(initDate);
        this.boughtFinalDate.setText(finalDate);
        this.boughtCat.setText(cat);
        setRate(rate);
    }

}
