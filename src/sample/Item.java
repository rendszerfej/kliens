package sample;

import Connection.Connection;
import Connection.messages.classes.Bid;
import Connection.messages.classes.Bidding;
import Connection.messages.classes.Product;
import Connection.messages.types.InsertMessage;
import Connection.messages.types.UpdateMessage;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import java.io.IOException;

public class Item extends Pane {
    /*
    @FXML
    Label itemPrice = new Label() ;
    @FXML
    Label itemOwner = new Label();
    @FXML
    Label itemDate= new Label();
    @FXML
    Label itemState= new Label();
    @FXML
    Label itemCat= new Label();
    @FXML
    TextArea itemDesc = new TextArea();
    //*/
    ///*
    @FXML
    Label itemPrice ;
    @FXML
    Label itemOwner;
    @FXML
    Label itemDate;
    @FXML
    Label itemState;
    @FXML
    Label itemCat;
    @FXML
    Label itemName;
    @FXML
    TextArea itemDesc;
    @FXML
    TextField itemBid;
    @FXML
    Button letsBid;
    @FXML
    Label onlyHigher;
    @FXML
    HBox licitHBox;
    @FXML
    Button getProfile;
    int bid;

    public Button getGetProfileButton() {
        return getProfile;
    }
    //*/

    GetCategory gc = new GetCategory();
    Product itemProduct;
    Bidding itemBidding;

    public void hideLicit(){
        this.licitHBox.visibleProperty().setValue(false);
    }
    public Bidding getItemBidding() {
        return itemBidding;
    }

    public void setItemBidding(Bidding itemBidding) {
        this.itemBidding = itemBidding;
        if(itemBidding.getFinal_price() > 0 && itemBidding.getFinal_price() > this.itemProduct.getPrice()){
            this.itemPrice.setText(String.valueOf(itemBidding.getFinal_price()));
        }
        else{
            this.itemPrice.setText(String.valueOf(this.itemProduct.getPrice()));
        }

        itemProduct.setPrice(itemBidding.getFinal_price());
    }

    public Item() {
        init();

    }
    public Item(Product a,String owner) {
        init();
        itemProduct = a;

        initData(a.getName(),String.valueOf(a.getPrice()),owner,a.getAdvertisement_date(),a.getStatus(),gc.getCategoryFromInt(a.getSub_category_id()),a.getDescription());


        if(Controller.loginUser!=null && owner.equals(Controller.loginUser.getEmail())){
            licitHBox.visibleProperty().setValue(false);
        }
        else{
            licitHBox.visibleProperty().setValue(true);
        }
    }

    public Product getItemProduct() {
        return itemProduct;
    }

    public  Item(String name, String price, String owner, String date, String state, String category, String description){
        init();
        initData( name,price,owner,date,state, category,description);
    }
    private void init(){
        FXMLLoader loader = new FXMLLoader(getClass().getResource("item.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (Exception e) {

        }

        this.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));

        itemBid.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                itemBid.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });


        letsBid.setOnAction(event -> {
            if(itemBid.visibleProperty().getValue()==false){
                if(itemBidding!=null)
                    itemBid.setText(String.valueOf((itemBidding.getFinal_price()>itemProduct.getPrice())?itemBidding.getFinal_price()+1:itemProduct.getPrice()+1));
                else
                    itemBid.setText(String.valueOf(itemProduct.getPrice()));
                itemBid.visibleProperty().setValue(true);
                onlyHigher.setText("Az árnál csak magasabb értéket fogad el!");
            }
            else{
                //System.out.println(itemBidding.toString());
                System.out.println(Controller.loginUser.toString());
                int bid = Integer.parseInt(itemBid.getText());

                if(Controller.loginUser!=null && itemBidding!=null){
                    if(bid < itemProduct.getPrice()){
                        onlyHigher.visibleProperty().setValue(true);
                    }
                    else {
                        Connection conn = Connection.getInstance();
                        InsertMessage im = new InsertMessage();
                        UpdateMessage um = new UpdateMessage();
                        Bidding bidding = new Bidding();
                        bidding.setProduct_id(this.itemProduct.getProduct_id());
                        bidding.setFinal_price(bid);
                        um.setContent(bidding);
                        Bid b = new Bid();
                        b.setBidding_id(itemBidding.getBidding_id());
                        b.setBidder_id(Controller.loginUser.getProfile_id());
                        b.setBid_value(bid);
                        im.setContent(b);
                        try {
                            conn.send(im);
                            conn.send(um);

                            itemPrice.setText(String.valueOf(bid));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                else{
                    onlyHigher.setText("Nem vagy bejelentkezve!");

                }


            }
        });

    }

    public void setItemPrice(String price){
        this.itemPrice.setText(price);
    }

    public void initData(String name,String price,String owner,String date,String state,String category,String description){
        this.itemName.setText(name);
        this.itemPrice.setText(price);
        this.itemOwner.setText(owner);
        this.itemDate.setText(date);
        this.itemState.setText(state);
        this.itemCat.setText(category);
        this.itemDesc.setText(description);
    }
    private void loadFXML(){

    }


}
