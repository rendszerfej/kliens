package sample;

public enum categories {
    Muszaki_Mob("Műszaki cikk", "Mobiltelefon",1,1),
    Muszaki_TV("Műszaki cikk","TV",1,2),
    Muszaki_Ful("Műszaki cikk","Fülhallgató, fejhallgató",1,3),
    SzT1("Számítógép","Notebook",2,4),
    SzT2("Számítógép","Nyomtató",2,5),
    SzT3("Számítógép","Monitor",2,6),
    HztGp1("Háztartási gép","Porszívó",3,7),
    HztGp2("Háztartási gép","Hűtőszekrény",3,8),
    HztGp3("Háztartási gép","Mosógép",3,9),
    DandR1("Divat és ruházat","Cipő",4,10),
    DandR2("Divat és ruházat","Kabát",4,11),
    DandR3("Divat és ruházat","Táska",4,24),
    SandF1("Sport és fitness","Kerékpár",5,13),
    SandF2("Sport és fitness","Gördeszka",5,25),
    J1("Játék","Lego",8,15),
    J2("Játék","Társasjáték",8,16),
    J3("Játék","Puzzle",8,17),
    OandK("Otthon és kert","Fűnyíró",6,26),
    AM1("Autó, motor","Motor bukósisak",7,27),
    AM2("Autó, motor","Autó akkumulátor",7,20),
    Iroda1("Irodatechnika","Fénymásoló",9,21),
    Iroda2("Irodatechnika","Forgószék",9,22),
    Iroda3("Irodatechnika","Számológép",9,23);


    private String maincategory;
    private String subcategory;
    private int main,sub;

    categories(String maincat,String subcat,int main,int sub){
        this.maincategory=maincat;
        this.subcategory=subcat;
        this.main=main;
        this.sub=sub;
    }
    public String toString(){
        return maincategory+" - "+subcategory;
    }

    public String getMaincategory() {
        return maincategory;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public int getMain() {
        return main;
    }

    public String getCategoryFromInt(int sub){
        switch (sub){
            case 1:{
                return this.toString();
            }
            case 2:{
                return this.toString();
            }
            case 3:{
                return this.toString();
            }
            case 4:{
                return this.toString();
            }
            default:
                return "Nincs ilyen kategória";
        }
    }
    public int getSub() {
        return sub;
    }
}
