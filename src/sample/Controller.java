package sample;

import Connection.Connection;
import Connection.messages.classes.*;
import Connection.messages.messageboard.AckMessage;
import Connection.messages.messageboard.ErrorMessage;
import Connection.messages.messageboard.ListMessage;
import Connection.messages.types.*;
import javafx.application.Platform;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Orientation;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import repository.ProductRepository;
import repository.Repository;

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Array;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Controller implements OtherUsers {

    @FXML
    Button login;
    @FXML
    Button reg ;
    @FXML
    Button profilMenu;
    @FXML
    Button itemsMenu;
    @FXML
    Button bidsButton;
    @FXML
    Button boughtsButton;
    @FXML
    Button wishlistMenu;
    @FXML
    Button logout;
    @FXML
    Button listItems;
    @FXML
    TextField searchField;
    @FXML
    TextField username;
    @FXML
    TextField password;
    OldConnection client;
    @FXML
    VBox loginBox = new VBox();
    @FXML
    VBox regBox = new VBox();
    @FXML
    Button registNewUser = new Button();
    @FXML
    VBox leftSidePanel = new VBox();
    /*@FXML
    StackPane logBox;

     */
    @FXML
    TextField loginUsername;
    @FXML
    PasswordField loginPassword;
    @FXML
    TextField regUsername;
    @FXML
    PasswordField regPassword;
    @FXML
    TextField lastName;
    @FXML
    TextField firstName;
    @FXML
    TextField Residence;
    @FXML
    TextField email;
    @FXML
    TextField telephone;
    @FXML
    FlowPane itemsPane = new FlowPane();
    @FXML
    Group center = new Group();
    @FXML
    Label userName;

    @FXML
    Pane mainPane;
    @FXML
    ScrollPane scrollPane = new ScrollPane();
    @FXML
    BorderPane root;



    //profil fxml
    @FXML
    Pane profileMain;
    @FXML
    VBox baseData;
    @FXML
    Button showBaseData;
    @FXML
    Pane profileBase;

    //Termékek fxml
    @FXML
    ScrollPane itemsScrollPane;
    @FXML
    FlowPane itemsFlowPane;

    //ajánló
    @FXML
    ScrollPane recScrollPane;
    @FXML
    FlowPane recFlowPane;
    //új termék
    @FXML
    ComboBox<categories> categoriesComboBox;
    @FXML
    TextField newItemName;
    @FXML
    TextField newItemPrice;
    @FXML
    TextField newItemState;
    @FXML
    TextArea newItemDesc;
    @FXML
    Button newItemUpload;
    //whishlist
    @FXML
    Button saveWshList;
    @FXML
    VBox wishlistLeftPanel;
    @FXML
    CheckBox MainCatMuszaki;
    @FXML
    CheckBox mobil;
    @FXML
    CheckBox wishlistTV;
    @FXML
    CheckBox fejhall;
    @FXML
    VBox muszaki;
    //profil
    @FXML
    Label profileLastName;
    @FXML
    Label profileFirstName;
    @FXML
    Label profileCity;
    @FXML
    Label profileEmailAddress;
    @FXML
    Label profilePhoneNumber;

    public static Profile loginUser;

    ArrayList<Bidding> biddings = new ArrayList<>();
    ArrayList<Item> testitems=new ArrayList<>();
    ArrayList<Item> myItems = new ArrayList<>();
    ArrayList<Bid> myBids = new ArrayList<>();
    ArrayList<Item> other = new ArrayList<>();
    ArrayList<Product> products = new ArrayList<>();
    ArrayList<Integer> categorylist = new ArrayList<>();
    ArrayList<Integer> mybiddings=new ArrayList<>();
    Wishlist userWishlist=null;
    ArrayList<Item> soldItems=new ArrayList<>();

    ArrayList<Review> userReviews = new ArrayList<>();


    private Connection conn = Connection.getInstance();
    public static int loggedIn = 0;


    GetUser gu=new GetUser();
    public Profile getLoginUser() {
        return loginUser;
    }

    private Repository productRepository = new ProductRepository();

    @FXML
    private void initialize() {
        init();
        connect("localhost", 3333);
        //teszttermekek();
        loginUsername.setText("abel@proba.com");
        loginPassword.setText("abel123");

    }

    private void init() {

        reg.setOnAction(event -> {
            mainPane.getChildren().clear();
            mainPane.getChildren().add(regBox);
            //
            numberOnly(telephone);
            //loginBox.visibleProperty().setValue(false);
            regBox.visibleProperty().setValue(true);
        });

        //check login status

        notLoggedIn();

    }

    private void connectToServer() {

        try {
            client = new OldConnection("127.0.0.1", 5000);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    public void registerNewUser() {
        String password = regPassword.getText();
        String lastname = lastName.getText();
        String firstname = firstName.getText();
        String residence = Residence.getText();
        String eMail = email.getText();
        String phone = telephone.getText();



        //regisztráció rögzítése
        if (!(password.equals("") || lastname.equals("") || firstname.equals("") || residence.equals("") || eMail.equals("") || phone.equals(""))) {

            InsertMessage im = new InsertMessage();
            Profile p = new Profile();
            p.setFirst_name(firstname);
            p.setLast_name(lastname);
            p.setEmail(eMail);
            p.setPhone_number(phone);
            p.setPassword(digest("SHA1",password));
            p.setCity(residence);
            p.setProfile_id(1);

            im.setContent(p);
            try {
                conn.send(im);
            } catch (IOException e) {
                e.printStackTrace();
            }

            mainPane.getChildren().clear();
            mainPane.getChildren().add(loginBox);
            //loginBox.getChildren().add(logBox);
            regBox.visibleProperty().setValue(false);
            loginBox.visibleProperty().setValue(true);

        } else {

        }

    }

    private void listAllUsers(){
        SelectMessage sm = new SelectMessage();
        Profile p = new Profile();
        sm.setContent(p);
        try {
            conn.send(sm);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void login() {
        /**
         * A login gombot megnyomva elküldjük a server-nek, hogy törölje a paraméterben kapott terméket.
         * Ez persze még nem fogja törölni, mert:
         *      -nincs is még ilyen termékünk
         *      -a serveren nincsnek fent az SQL utasítások
         * De a server (mivel lehetne ilyent csinálni) visszaküld egy AckMessage típusú üzenetet,
         * ezzel jelzi, hogy feldolgozta és ez lehetséges művelet.
         */
        try {
            /**
             * LoginMessage azért szükséges, mert ez elküldi a client id-t (email) is.
             * Amikor a server egy LoginMessage-t fogad, akkor regisztrálja egy map-be a clientID-t(email) és az
             * ObjectOutputStream-jét, így tudunk a szerverről egy önálló üzenetet küldeni, értesítés céljából.
             */


            LoginMessage lm = new LoginMessage();
            String eMail = loginUsername.getText();

            String password = loginPassword.getText();
            String hashedPassword = digest("SHA1", password);

            Profile p = new Profile();
            p.setEmail(eMail);
            p.setPassword(hashedPassword);
            lm.setContent(p);
            lm.setClientId(p.getEmail());
            conn.send(lm);
            //listItems();


            /**
             * Amikor egy olyan InsertMessage-t, aminek a tartalma egy Product, a server feldolgoz, NotifyMessage-ben
             * elküldi az összes olyan csatlakozott client-nek, aki érdeklődik a termék kategóriája iránt.
             * Ez erre egy példa, itt még visszaküldi saját magának is, hogy könnyen lehessen ellenőrizni.
             */
            //Sikeres küldés, értesítést is kapott, mert phpMyAdmin-ba hozzáadtam az első felhasználóhoz az 1. alkategóriát
//            InsertMessage im = new InsertMessage();
//            im.setContent(new Product(5, 11, 1, 89800, "2020-04-09 23:11:27", "Huawei P40 Lite mobitelefon", "Mobiltelefon - 6,4\"-es kijelző, 2310 × 1080, IPS kijelző, 8 magos processzor, 6 GB RAM, 128 GB belső tárhely", "Bontatlan"));
//            conn.send(im);

            //Itt nem kap értesítést
//            InsertMessage im = new InsertMessage();
//            im.setContent(new Product(5, 11, 2, 89800,"2020-04-09 23:11:27", "Huawei P40 Lite mobitelefon", "Mobiltelefon - 6,4\"-es kijelző, 2310 × 1080, IPS kijelző, 8 magos processzor, 6 GB RAM, 128 GB belső tárhely", "Bontatlan"));
//            conn.send(im);
            //Licit értesítés
            /*InsertMessage im = new InsertMessage();
            im.setContent(new Bid(2, 11,1,500));
            conn.send(im);*/
            //ha sikerült belépni
             SelectMessage sm = new SelectMessage();
            sm.setContent(p);
            conn.send(sm);
            loggedIn = 1;

            userName.setText(eMail);

        } catch (IOException e) {
            e.printStackTrace();
        }

        //check login status
        notLoggedIn();
    }

    public void notLoggedIn() {
        if (loggedIn == 0) {
            logout.setText("Bejelentkezés");
            profilMenu.setDisable(true);
            itemsMenu.setDisable(true);
            bidsButton.setDisable(true);
            boughtsButton.setDisable(true);
            wishlistMenu.setDisable(true);
        }
        if (loggedIn == 1) {
            //nem mukodik meg
            if(logout.isPressed()){
                loggedIn = 0;
            }

            logout.setText("Kijelentkezés");
            profilMenu.setDisable(false);
            itemsMenu.setDisable(false);
            bidsButton.setDisable(false);
            boughtsButton.setDisable(false);
            wishlistMenu.setDisable(false);
        }
    }

    //hashes szükséges függvény1
    private static String encodeHex(byte[] digest) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < digest.length; i++) {
            sb.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

    //hashes szükséges függvény2
    public static String digest(String alg, String input) {
        try {
            MessageDigest md = MessageDigest.getInstance(alg);
            byte[] buffer = input.getBytes("UTF-8");
            md.update(buffer);
            byte[] digest = md.digest();
            return encodeHex(digest);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    /**
     * Csatlakozás a server socket-hez
     *
     * @param address a server socket IP címe
     * @param port    a server socket portja
     */
    private void connect(String address, int port) {
        conn.setServer(address);
        conn.setPort(port);
        conn.startConnection();
        /**
         * OnReceiveCallback-re azért van szükség, hogy megadjunk egy függvényt,
         * ami a server-től visszakapott üzenetet, feldolgozza.
         * Ebben az esetben megnézzük az üzenet típusát és annak megfelelően adunk egy visszajelzést.
         * A Platform.runLater(Runnable) azért kell, hogy az előbbi feldolgozó függvényünk elinduljon a háttérben,
         * majd miután elindult, visszatérjen a program a GUI-hoz.
         */
        conn.setOnReceiveCallback(data ->
                Platform.runLater(() -> {
                    if (data instanceof AckMessage) {
                        System.out.println("Operation succeed!");
                        listBids();
                        listAllUsers();
                        listReviews();
                        listProducts();
                    }
                    else if (data instanceof ErrorMessage)
                        System.out.println("Error: " + ((ErrorMessage) data).getErrorMsg());
                    else if (data instanceof NotifyBidMessage){
                        System.out.println("Hey! Are you poor or what? Your bid has been exceeded! Here is the product: \n" + ((NotifyBidMessage) data).getContent().toString());
                        //listItems();
                        listProducts();
                    }
                    else if (data instanceof NotifyProductMessage)
                        System.out.println("Hey! There is a new product! Buy it now and give me your money!\n" + ((NotifyProductMessage) data).getContent().toString());
                    else if(data instanceof Profile){
                        Profile prof = (Profile)data;
                        loginUser.setFirst_name(prof.getFirst_name());
                        System.out.println(data.toString());
                        //loginUser=;
                    }

                    else if (data instanceof ListMessage) {
                        try {
                            Serializable item = ((ListMessage) data).getSerializables().get(0);

                            ArrayList<IItem> itemArrayList = new ArrayList<>();

                            for (Serializable serializable : ((ListMessage) data).getSerializables()) {
                                itemArrayList.add((IItem) serializable);
                            }
                            if (item.getClass() == Product.class) {
                                products.clear();
                                productRepository.addAll(itemArrayList);
                                for (IItem i : itemArrayList) {

                                    products.add((Product) i);
                                }
                                listBiddings();
                            }
                            else if (item.getClass() == Bidding.class) {
                                biddings.clear();
                                for (IItem i : itemArrayList) {
                                    biddings.add((Bidding) i);
                                }

                            }
                            else if (item.getClass() == Bid.class) {
                                myBids.clear();
                                mybiddings.clear();
                                for (IItem i : itemArrayList) {
                                    Bid b =(Bid) i;
                                    if(loginUser.getProfile_id()==b.getBidder_id())
                                    {
                                        mybiddings.add(b.getBidding_id());
                                    }
                                        myBids.add(b);
                                }
                                //System.out.println("mybids num:"+myBids.size());

                            }
                            else if(item.getClass() == Review.class){
                                userReviews.clear();
                                for(IItem i :itemArrayList){
                                    Review review= (Review)i;
                                    userReviews.add(review);

                                }
                            }
                            else if (item.getClass() == Profile.class) {
                                if(itemArrayList.size()==1){
                                loginUser = (Profile) item;
                                listProducts();
                                getUserWishList();
                                listBids();
                                listAllUsers();
                                listReviews();
                                }
                                else{
                                    otherUsers.clear();
                                    for (IItem i : itemArrayList) {
                                        Profile p = (Profile)i;
                                        otherUsers.add(p);
                                    }
                                    System.out.println("felhasználók:"+otherUsers.size());

                                }
                            }
                            else if(item.getClass() == Wishlist.class){
                                for(IItem i : itemArrayList){
                                    Wishlist a = (Wishlist)i;
                                    if(a.getProfile_id()==loginUser.getProfile_id()){
                                        userWishlist=a;
                                        loadWishListCategories();
                                        break;
                                    }
                                }
                            }
                            else if (item.getClass() == CategoryWishlist.class) {
                                categorylist.clear();
                                for(IItem i : itemArrayList){
                                    CategoryWishlist cw = (CategoryWishlist)i;
                                    if(cw.getWishlist_id()==userWishlist.getWishlist_id()){
                                        categorylist.add(cw.getSub_category_id());
                                    }


                                }
                                loadRecommndation();

                            }

                        }catch (Exception e)
                        {
                            System.out.println("Eredménylista üres");

                            if(loggedIn==1){
                                //loadItemsMenu();
                                listProducts();
                            }
                            //loadRecommndation();
                        }
                    }
                    else if (data instanceof LoginMessage) {
                        System.out.println(((LoginMessage) data).getContent());
                    }

                }));
    }
    private void listReviews(){
        SelectMessage sm = new SelectMessage();
        Review review = new Review();
        sm.setContent(review);
        try {
            conn.send(sm);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void loadItemsMenu() { //termékek menü
        loadUI("items");
        Button newItem = (Button) mainPane.lookup("#newItem");
        Button myItemsList = (Button) mainPane.lookup("#myItemsList");
        Pane itemsMain = (Pane) mainPane.lookup("#ItemsMain");
        itemsMain.getChildren().clear();
        itemsMain.getChildren().add((Pane) loadUINode("myItemsList"));
        listMyItems(itemsMain); // bejelentkezett felhasználó termékei

        newItem.setOnAction(event -> {
            itemsMain.getChildren().clear();
            itemsMain.getChildren().add((Pane) loadUINode("newItem"));
            categoriesComboBox=(ComboBox)mainPane.lookup("#categoriesComboBox");
            newItemName=(TextField)mainPane.lookup("#newItemName");
            newItemPrice=(TextField)mainPane.lookup("#newItemPrice");
            newItemState=(TextField)mainPane.lookup("#newItemState");
            newItemDesc=(TextArea)mainPane.lookup("#newItemDesc");
            newItemUpload=(Button)mainPane.lookup("#newItemUpload");
            numberOnly(newItemPrice);


            fillCombobox(categoriesComboBox);
            BooleanBinding bb = new BooleanBinding() {
                {
                    super.bind(newItemDesc.textProperty(),newItemPrice.textProperty(),newItemState.textProperty(),newItemName.textProperty());
                }
                @Override
                protected boolean computeValue() {
                    return (newItemDesc.getText().isEmpty()
                            || newItemPrice.getText().isEmpty()
                            || newItemState.getText().isEmpty()
                            || newItemName.getText().isEmpty()
                            || categoriesComboBox.getSelectionModel().isEmpty());
                }

            };
            newItemUpload.disableProperty().bind(bb);

            newItemUpload.setOnAction(event1 -> {
                //új termék beszúrása az adatbázisba
                //comboboxból lekérhető a fő és alkategória szövegként vagy id-ként, pl: categoriesComboBox.getValue().getMain();
                categories a = categoriesComboBox.getValue();
                try{
                    createNewItem(newItemName.getText(),newItemPrice.getText(),newItemState.getText(),a.getSub(),newItemDesc.getText());

                    int productId = -1;

                    SelectMessage sm = new SelectMessage();
                    Product p = new Product();
                    if (loginUser != null) {
                        p.setProfile_id(loginUser.getProfile_id());
                        sm.setContent(p);
                        conn.send(sm);
                    } else {
                        System.out.println("Error getting product id");
                    }

                    productId = products.get(0).getProduct_id() + 1;

                    createNewBidding(productId);


                }catch (Exception e){

                }
            });

        });
        myItemsList.setOnAction(event -> {
            itemsMain.getChildren().clear();
            itemsMain.getChildren().add((Pane) loadUINode("myItemsList"));
            listMyItems(itemsMain);
        });

    }
    private void createNewItem(String name,String price,String state,int subcategory,String description) throws Exception {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        String timestamp= dateFormat.format(date);


        InsertMessage im = new InsertMessage();
        int pr = Integer.parseInt(price);
        im.setContent(new Product(-1, loginUser.getProfile_id(), subcategory, pr, timestamp, name, description, state));
        conn.send(im);


        //Item a = new Item(name,price,userName.getText(),timestamp,state,category,description);
        //testitems.add(a);
    }

    private void createNewBidding(int id) throws Exception {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        String timestamp= dateFormat.format(date);

        long time = System.currentTimeMillis() + 604800000; // Egy héttel később
        Date dateend = new Date(time);
        String timestampend = dateFormat.format(dateend);

        InsertMessage im = new InsertMessage();
        im.setContent(new Bidding(-1, id, -1, timestamp, timestampend));
        conn.send(im);
    }


    private void loadRecommndation(){

        loadUI("recommendation");
        ScrollPane recScrollPane = (ScrollPane) mainPane.lookup("#recScrollPane");
        FlowPane recFlowPane = (FlowPane) recScrollPane.getContent();

        recFlowPane.getChildren().clear();
        //legfrissebb termékek a kivánságlistáról
        System.out.println(products.size());
        if(categorylist.size()!=0){
            for(int i = 0;i < categorylist.size();i++){
                for(Product p : products){
                    if(p.getSub_category_id()==categorylist.get(i) && (p.getProfile_id()!=loginUser.getProfile_id())){
                        Item item = new Item(p,gu.getUserFromId(p.getProfile_id()));
                        for(Bidding b : biddings){
                            if(item.getItemProduct().getProduct_id()==b.getProduct_id()){
                                item.setItemBidding(b);
                                break;
                            }
                        }
                        recFlowPane.getChildren().add(item);
                        break;
                    }
                }
            }
        }
        else{
            Label text = new Label("Kivánságlistád üres!");
            text.setStyle("-fx-font-size: 28;");
            recFlowPane.getChildren().add(text);
        }



        recFlowPane.setOrientation(Orientation.HORIZONTAL);
    }
    private void fillCombobox(ComboBox cb){
        cb.setPromptText("Kérem válasszon kategóriát!");
        cb.getItems().setAll(categories.values());
    }

    public void logout(){
        mainPane.getChildren().clear();
        mainPane.getChildren().add(loginBox);
        loginUser=null;
        myItems.clear();
        categorylist.clear();
        userWishlist=null;

        loggedIn = 0;
        init();

        loginBox.visibleProperty().setValue(true);
        regBox.visibleProperty().setValue(false);
    }

    private void listProducts(){
        Product p = new Product();
        SelectMessage sm = new SelectMessage();
        //p.setProfile_id();
        sm.setContent(p);
        try {
            conn.send(sm);
        } catch (IOException e) {
            e.printStackTrace();
        }
        listBiddings();
    }
    private void listBids(){
        Bid p = new Bid();
        SelectMessage sm = new SelectMessage();
        //p.setProfile_id();
        sm.setContent(p);
        try {
            conn.send(sm);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void listBiddings() {
        Bidding b = new Bidding();
        SelectMessage sm = new SelectMessage();
        //p.setProfile_id();
        sm.setContent(b);
        try {
            conn.send(sm);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void listMyItems(Pane itemsMain) { //felhasználó által feltöltött termékek, lekérdezés kell
        ScrollPane myItemsScrollPane = (ScrollPane) itemsMain.lookup("#myItemsScrollPane");
        FlowPane myItemsFlowPane = (FlowPane) myItemsScrollPane.getContent();
        myItemsFlowPane.getChildren().clear();
        //teszt termékek
        /*for(int i = 0;i < testitems.size();i++){

            myItemsFlowPane.getChildren().add(testitems.get(i));
        }*/

        //System.out.println(myItems.size()+" "+other.size()+" "+products.size());
        for(int i = 0;i < products.size();i++){
            if(products.get(i).getProfile_id()==loginUser.getProfile_id()){
                Item a = new Item(products.get(i),loginUser.getEmail());

                myItemsFlowPane.getChildren().add(a);
            }

        }

        //felhasználó termékeinek listázása
        try{
            SelectMessage sm = new SelectMessage();
            Product p = new Product();
            if (loginUser != null) {
                p.setProfile_id(loginUser.getProfile_id());
                sm.setContent(p);
                conn.send(sm);
            } else {
                System.out.println("login to list your products");
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }



        myItemsFlowPane.setOrientation(Orientation.HORIZONTAL);
        //myItemsFlowPane.getChildren().addAll(e, f, g, h); //
    }

    /*private void ifNoWishlist(){
        if(userWishlist==null){
            InsertMessage im = new InsertMessage();
            Wishlist w = new Wishlist();
            w.setProfile_id(loginUser.getProfile_id());
            w.setWishlist_id(loginUser.getProfile_id());
            im.setContent(w);
            try {
                conn.send(im);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }*/

    @FXML
    public void wishlistMenu() { // kivánságlista listázása
        //if(mainPane.getChildren().contains(wishlistBase))
        loadUI("wishlist");

        saveWshList=(Button)mainPane.lookup("#saveWshList");
        ArrayList<CheckBox> cbs = new ArrayList<>();
        ArrayList<Boolean> start = new ArrayList<>();
        wishlistTV = (CheckBox) mainPane.lookup("#2");

        cbs.add(wishlistTV);
        fejhall=(CheckBox)mainPane.lookup("#3");
        cbs.add(fejhall);
        mobil=(CheckBox)mainPane.lookup("#1");
        cbs.add(mobil);
        CheckBox nyomtato = (CheckBox)mainPane.lookup("#6");
        cbs.add(nyomtato);
        CheckBox monitor = (CheckBox)mainPane.lookup("#5");
        cbs.add(monitor);
        CheckBox notebook = (CheckBox)mainPane.lookup("#4");
        cbs.add(notebook);
        CheckBox porszivo = (CheckBox)mainPane.lookup("#7");
        cbs.add(porszivo);
        CheckBox huto = (CheckBox)mainPane.lookup("#8");
        cbs.add(huto);
        CheckBox mosogep = (CheckBox)mainPane.lookup("#9");
        cbs.add(mosogep);
        CheckBox cipo = (CheckBox)mainPane.lookup("#10");
        cbs.add(cipo);
        CheckBox kabat = (CheckBox)mainPane.lookup("#11");
        cbs.add(kabat);
        CheckBox taska = (CheckBox)mainPane.lookup("#24");
        cbs.add(taska);
        CheckBox kerekpar = (CheckBox)mainPane.lookup("#13");
        cbs.add(kerekpar);
        CheckBox gordeszka = (CheckBox)mainPane.lookup("#25");
        cbs.add(gordeszka);
        CheckBox funyiro = (CheckBox)mainPane.lookup("#26");
        cbs.add(funyiro);
        CheckBox akksi = (CheckBox)mainPane.lookup("#20");
        cbs.add(akksi);
        CheckBox bukosisak = (CheckBox)mainPane.lookup("#27");
        cbs.add(bukosisak);
        CheckBox lego = (CheckBox)mainPane.lookup("#15");
        cbs.add(lego);
        CheckBox tarsas = (CheckBox)mainPane.lookup("#16");
        cbs.add(tarsas);
        CheckBox puzzle = (CheckBox)mainPane.lookup("#17");
        cbs.add(puzzle);
        CheckBox fenymasolo = (CheckBox)mainPane.lookup("#21");
        cbs.add(fenymasolo);
        CheckBox forgoszek = (CheckBox)mainPane.lookup("#22");
        cbs.add(forgoszek);
        CheckBox szamologep = (CheckBox)mainPane.lookup("#23");
        cbs.add(szamologep);

        System.out.println(categorylist.size());
        for(CheckBox cb : cbs){
            //System.out.println();

            if(categorylist.contains(Integer.parseInt(cb.getId()))){
                cb.setSelected(true);
                start.add(true);
            }
            else{
                start.add(false);
            }
            //addListenerToCheckBox(cb);
        }




        /*InsertMessage im = new InsertMessage();
        Wishlist insertWishlist = new Wishlist();
        CategoryWishlist cw = new CategoryWishlist();
        cw.setSub_category_id(3);
        cw.setWishlist_id(1);
        insertWishlist.setProfile_id(loginUser.getProfile_id());
        insertWishlist.setWishlist_id(loginUser.getProfile_id());*/

        saveWshList.setOnAction(event -> {
            try {
                insertIntoWishListCategory(cbs,start);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        System.out.println(cbs.size());




    }
    private void insertIntoWishListCategory(ArrayList<CheckBox> cbs,ArrayList<Boolean> start) throws IOException {
        for(int i = 0;i < cbs.size();i++){
            if(!cbs.get(i).isSelected()==start.get(i)){
                if(start.get(i)==false){
                    InsertMessage im = new InsertMessage();
                    CategoryWishlist cw = new CategoryWishlist();
                    cw.setSub_category_id(Integer.parseInt(cbs.get(i).getId()));
                    cw.setWishlist_id(userWishlist.getWishlist_id());
                    im.setContent(cw);
                    conn.send(im);
                }
                else{
                    DeleteMessage dm = new DeleteMessage();
                    CategoryWishlist cw = new CategoryWishlist();
                    cw.setSub_category_id(Integer.parseInt(cbs.get(i).getId()));
                    cw.setWishlist_id(userWishlist.getWishlist_id());
                    dm.setContent(cw);
                    conn.send(dm);
                }
            }
        }
        loadWishListCategories();

    }

    private void getUserWishList(){
        SelectMessage sm = new SelectMessage();
        Wishlist w = new Wishlist();
        w.setProfile_id(loginUser.getProfile_id());
        sm.setContent(w);
        try {
            conn.send(sm);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void loadWishListCategories() {
        SelectMessage sm = new SelectMessage();
        CategoryWishlist cw = new CategoryWishlist();
        cw.setWishlist_id(userWishlist.getWishlist_id());
        sm.setContent(cw);

        try {
            conn.send(sm);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    @FXML
    public void profil() { //felhasználó adatait le kell kérni
        if (!mainPane.getChildren().contains(profileBase)) {
            loadUI("profil");
        }
        profileLastName=(Label)mainPane.lookup("#profileLastName");
        profileFirstName=(Label)mainPane.lookup("#profileFirstName");
        profileCity=(Label)mainPane.lookup("#profileCity");
        profileEmailAddress=(Label)mainPane.lookup("#profileEmailAddress");
        profilePhoneNumber=(Label)mainPane.lookup("#profilePhoneNumber");

        if(loginUser!=null){
            profileLastName.setText(loginUser.getFirst_name());
            profileFirstName.setText(loginUser.getLast_name());
            profileCity.setText(loginUser.getCity());
            profileEmailAddress.setText(loginUser.getEmail());
            profilePhoneNumber.setText(loginUser.getPhone_number());
        }
        else
        {
            System.out.println("Profile not set");
        }

    }

    @FXML
    public void listItems() { // a keresőbe beírt kategória alapján listázni a termékeket

        loadUI("listing");
        itemsScrollPane = (ScrollPane) mainPane.lookup("#itemsScrollPane");
        itemsFlowPane = (FlowPane) itemsScrollPane.getContent();
        itemsFlowPane.setOrientation(Orientation.HORIZONTAL);
        String search = searchField.getText();
        String lowerSearch = search.toLowerCase();
        soldItems.clear();

        if (loginUser == null) { //nem bejelentkezett
            try {
                /*Product p = new Product();
                SelectMessage sm = new SelectMessage();
                sm.setContent(p);
                conn.send(sm);*/
                for (Product pro: products) {
                    String lowerProductName = pro.getName().toLowerCase();
                    if(lowerProductName.matches(".*"+lowerSearch+".*")){
                        Item a = new Item(pro, gu.getUserFromId(pro.getProfile_id()));
                        for(Bidding b : biddings){
                            if(a.getItemProduct().getProduct_id()==b.getProduct_id()){
                                a.setItemBidding(b);
                                break;
                            }
                        }
                        Date itemdate = new SimpleDateFormat("yyyy-MM-dd").parse(a.getItemBidding().getEnd_bidding_time());
                        Date today = new Date();
                        if(itemdate.before(today)){
                            itemsFlowPane.getChildren().add(a);
                        }
                        else{
                            soldItems.add(a);
                        }
                    }
                }
            } catch ( ParseException e) {//IOException |
                System.out.println(e.getMessage());
            }
        }else{ //bejelentkezett
            try {
                /*Product p = new Product();
                SelectMessage sm = new SelectMessage();
                sm.setContent(p);
                conn.send(sm);*/
                for (Product pro: products) {
                    String lowerProductName = pro.getName().toLowerCase();
                    if(lowerProductName.matches(".*"+lowerSearch+".*")){
                        //sm.setContent(pro);
                        //conn.send(sm);
                        Item a = new Item(pro, gu.getUserFromId(pro.getProfile_id()));
                        for(Bidding b : biddings){
                            if(a.getItemProduct().getProduct_id()==b.getProduct_id()){
                                a.setItemBidding(b);
                                break;
                            }
                        }
                        Button profile = a.getGetProfileButton();
                        profile.setOnAction(event -> {
                            Profile p = gu.getProfileFromId(a.getItemProduct().getProfile_id());
                            loadUserProfile(p);
                        });

                        String itemDate=a.getItemBidding().getEnd_bidding_time();
                        //System.out.println(itemDate);
                        Date itemdate = new SimpleDateFormat("yyyy-MM-dd").parse(itemDate);
                        Date today = new Date();
                        if(itemdate.after(today)){
                            itemsFlowPane.getChildren().add(a);
                        }
                        else{
                            soldItems.add(a);
                        }


                    }
                }


            } catch (ParseException e) {//IOException |
                System.out.println(e.getMessage());
            }
        }

    }
    private void loadUserProfile(Profile p){
        loadUI("othersprofile");
        Label userLastName = (Label)mainPane.lookup("#userLastName");
        Label userFirstName = (Label)mainPane.lookup("#userFirstName");
        Label userEmailAddress = (Label)mainPane.lookup("#userEmailAddress");
        Label userPhoneNumber = (Label)mainPane.lookup("#userPhoneNumber");
        Label userCity = (Label)mainPane.lookup("#userCity");
        Label userRated = (Label)mainPane.lookup("#userRated");

        userLastName.setText(p.getLast_name());
        userFirstName.setText(p.getFirst_name());
        userEmailAddress.setText(p.getEmail());
        userPhoneNumber.setText(p.getPhone_number());
        userCity.setText(p.getCity());
        ArrayList<Review> thisUserReview= new ArrayList<>();
        for(Review r : userReviews){
            if(r.getProfile_id()==p.getProfile_id()){
                thisUserReview.add(r);
            }
        }
        float avg =0;
        if(thisUserReview.size()>0){
            float sum=0;
            for(Review review:thisUserReview){
                sum+=review.getReview_value();
            }
            avg=sum/thisUserReview.size();
            userRated.setText(String.valueOf(avg));
        }
        else {
            userRated.setText("A felhasználót még nem értékelték!");
        }







    }
    /*private void listBoughtItems(){
        System.out.println("termekek szurese");
        try{
        for (Product pro: products) {
                Item a = new Item(pro, String.valueOf(pro.getProfile_id()));
                for(Bidding b : biddings){
                    if(a.getItemProduct().getProduct_id()==b.getProduct_id()){
                        a.setItemBidding(b);
                        break;
                    }
                }
                String itemDate=a.getItemBidding().getEnd_bidding_time();
                System.out.println(itemDate);
                Date itemdate = new SimpleDateFormat("yyyy-MM-dd").parse(itemDate);
                Date today = new Date();
                if(itemdate.after(today)){
                    itemsFlowPane.getChildren().add(a);
                }
                else{
                    soldItems.add(a);
                }


            }
            System.out.println(soldItems.size());
        }
        catch(Exception e)
        {
        }

    }*/

    @FXML
    public void bidding() { //jelenleg futó liciteket kell listázni
        loadUI("bids");
        ScrollPane bidScrollPane = (ScrollPane) mainPane.lookup("#bidScrollPane");
        FlowPane bidFlowPane = (FlowPane) bidScrollPane.getContent();

        //teszt licitek
        //bidFlowPane.getChildren().clear();

        ArrayList<Bido> bidos = new ArrayList<>();
        System.out.println(biddings.size());
        for(int i = 0;i < biddings.size();i++){
            if(mybiddings.contains(biddings.get(i).getBidding_id())){
                Bido bido = new Bido();
                int bidID=biddings.get(i).getBidding_id();
                String termek_nev="";
                int mybid=0,highest_bid=0;
                for(Bid b :myBids){
                    if(bidID==b.getBidding_id()){
                        if(b.getBid_value()>highest_bid){
                            highest_bid=b.getBid_value();
                        }
                        if(b.getBidder_id()==loginUser.getProfile_id() && b.getBid_value()>mybid){
                            mybid=b.getBid_value();
                        }
                    }
                }
                for(Product p : products){
                    if(p.getProduct_id()==biddings.get(i).getProduct_id()){
                        termek_nev=p.getName();
                    }
                }
                bido.initBidData(termek_nev,mybid,highest_bid,biddings.get(i).getProduct_id(),bidID);
                //bido.setBiddinID();
                //bido.setProductID();



                Date itemdate = null;
                try {
                    itemdate = new SimpleDateFormat("yyyy-MM-dd").parse(biddings.get(i).getEnd_bidding_time());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Date today = new Date();
                if(itemdate.after(today)) {
                    bidos.add(bido);
                }

            }


        }
        if(bidos.size()==0){
            Label l = new Label("Jelenleg nem licitálsz termékekre!");
            l.setStyle("-fx-font-size: 26;");
            bidFlowPane.getChildren().add(l);
        }
        else
        {
            for(Bido b : bidos){
                bidFlowPane.getChildren().add(b);
            }
        }


        /*Bido a = new Bido("Mosógép", "1200", "1201");
        Bido b = new Bido("Diploma", "1", "2");
        Bido c = new Bido("Diploma", "1", "2");
        Bido d = new Bido("Diploma", "1", "2");
        Bido e = new Bido("Diploma", "1", "2");
        Bido f = new Bido("Diploma", "1", "2");
        Bido g = new Bido("Diploma", "1", "2");
        Bido h = new Bido("Diploma", "1", "2");
        Bido i = new Bido("Diploma", "1", "2");
        Bido j = new Bido("Diploma", "1", "2");*/



        //bidFlowPane.getChildren().addAll(a, b,c,d,e,f,g,h,i,j);
        //bidFlowPane.getChildren().addAll(a);
        bidFlowPane.setOrientation(Orientation.HORIZONTAL);

    }

    @FXML
    public void boughts() { // a vásárolt termékeket kellene listázni
        listItems();
        //listAllUsers();
        listReviews();
        loadUI("boughts");
        ScrollPane bidScrollPane = (ScrollPane) mainPane.lookup("#boughtsScrollPane");
        FlowPane boughtsFlowPane = (FlowPane) bidScrollPane.getContent();

        ArrayList<Bought> boughtItems=new ArrayList<>();
        for(Item i:soldItems){

            if(mybiddings.contains(i.getItemBidding().getBidding_id())){
                Bought bought = new Bought();
                bought.setBoughtItem(i);
                for(Review r:userReviews){
                    if(r.getProduct_id()==bought.getBoughtItem().getItemProduct().getProduct_id()){
                        bought.setRate(r.getReview_value());
                    }
                }
                boughtItems.add(bought);
            }
        }


        for(Bought b :boughtItems){
            boughtsFlowPane.getChildren().add(b);
        }
        //a sikeres liciteket kellene itt listázni
       /* Bought a = new Bought("Pannon MSc Diploma","priceless","Pannon Egyetem","2020-05-07","2020-07-01","Egyéb",4);
        Bought b = new Bought("Pannon BSc Diploma","priceless","Pannon Egyetem","2020-05-07","2020-07-01","Egyéb",1);
        Bought c = new Bought("Pannon GTK-s Diploma","free","Pannon Egyetem","2020-05-07","2020-07-01","Egyéb",0);


        */

        //boughtsFlowPane.getChildren().addAll(a,b,c);
        boughtsFlowPane.setOrientation(Orientation.HORIZONTAL);

    }

    @FXML
    public void showBaseData() {
        profileMain.getChildren().clear();
        profileMain.getChildren().add(baseData);
    }

    @FXML
    public void exitApplication(ActionEvent event) {
        try {
            conn.stop();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Platform.exit();
    }

    private Node loadUINode(String ui) {
        Parent root = null;
        FXMLLoader loader = new FXMLLoader(getClass().getResource(ui + ".fxml"));
        try {
            root = loader.load();
        } catch (Exception e) {

        }
        return root;
    }

    private void loadUI(String ui) {
        Parent root = null;
        FXMLLoader loader = new FXMLLoader(getClass().getResource(ui + ".fxml"));
        try {
            root = loader.load();
        } catch (Exception e) {

        }
        mainPane.getChildren().clear();
        mainPane.getChildren().add(root);

    }
    private void numberOnly(TextField tf){
        tf.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                tf.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
    }

}
